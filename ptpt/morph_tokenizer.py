from typing import List

import transformers
import tokenizers
import torch

from tokenizers.pre_tokenizers import Whitespace


class MorphSegmentationTokenizer(transformers.PreTrainedTokenizerFast):
    """
    Following
    https://github.com/huggingface/tokenizers/issues/666#issuecomment-809608640,
    we subclass PreTrainedTokenizerFast so that our tokenizer plays nice with
    the rest of HF's ecosystem, like the `datasets` library
    """

    def __init__(self, tokenizer: tokenizers.Tokenizer):
        self._tokenizer = tokenizer
        # you should change these to match your own tokenizer
        self._mask_token = "[MASK]"
        self._mask_token_id = 3
        self._pad_token_id = 4
        self._padding_side = "right"

    @property
    def mask_token(self):
        return self._mask_token

    @property
    def mask_token_id(self):
        return self._mask_token_id

    @property
    def pad_token_id(self):
        return self._pad_token_id

    @property
    def padding_side(self):
        return self._padding_side

    def __call__(
        self,
        examples,
        pairs=None,
        return_tensors=False,
        return_offsets_mapping=False,
        return_special_tokens_mask=False,
        **_,
    ):
        """
        We ignore the keyword arguments, as they are most likely incompatible with
        the `encode` and `batch_decode` methods. We can afford to ignore them due to
        the ad-hoc nature of this implementation
        """
        if isinstance(examples, str):
            encoded: tokenizers.Encoding = self._tokenizer.encode(examples)
            data = {
                "input_ids": encoded.ids,
                "token_type_ids": encoded.type_ids,
                "attention_mask": encoded.attention_mask,
            }
            if return_special_tokens_mask:
                data["special_tokens_mask"] = encoded.special_tokens_mask
            if return_tensors:
                data = data = {k: torch.LongTensor(v) for k, v in data.items()}
            if return_offsets_mapping:
                data["offset_mapping"] = encoded.offsets
            return transformers.BatchEncoding(data=data)
        if pairs is not None:
            batch = self._tokenizer.encode_batch(list(zip(examples, pairs)))
        else:
            batch: List[tokenizers.Encoding] = self._tokenizer.encode_batch(examples)
        data = {
            "input_ids": [element.ids for element in batch],
            "token_type_ids": [element.type_ids for element in batch],
            "attention_mask": [element.attention_mask for element in batch],
        }
        if return_special_tokens_mask:
            data["special_tokens_mask"] = [
                element.special_tokens_mask for element in batch
            ]
        if return_offsets_mapping:
            data["offset_mapping"] = [element.offsets for element in batch]
        if return_tensors:
            data = data = {k: torch.LongTensor(v) for k, v in data.items()}
        return transformers.BatchEncoding(data=data)

    def pad(self, examples, **_):
        """
        The name here is misleading, since padding is handled by `enable_padding` from
        the Tokenizer class. This only converts the lists in the batch dictionary into
        Torch tensors, to fit in the DataCollatorForLanuageModelling code
        """
        data = {
            "input_ids": torch.LongTensor(
                [element["input_ids"] for element in examples]
            ),
            "token_type_ids": torch.LongTensor(
                [element["token_type_ids"] for element in examples]
            ),
            "attention_mask": torch.LongTensor(
                [element["attention_mask"] for element in examples]
            ),
        }
        if "special_tokens_mask" in examples[0]:
            data["special_tokens_mask"] = torch.LongTensor(
                [element["special_tokens_mask"] for element in examples]
            )
        return data

    def convert_tokens_to_ids(self, token):
        """
        This method is used by the data collator to retrieve the id of the "[MASK]"
        token, so we know beforehand we don't need to handle a list
        """
        return self._tokenizer.token_to_id(token)

    def save_pretrained(self, output_dir, **kwargs):
        self._tokenizer.pre_tokenizer = Whitespace()
        self._tokenizer.save(f"{output_dir}/tokenizer.json")

    def __len__(self):
        return self._tokenizer.get_vocab_size()
