from tokenizers import Regex, Tokenizer, decoders, normalizers, pre_tokenizers, processors

from .custom_tokenizer import CustomPretokenizer
from .morph_tokenizer import MorphSegmentationTokenizer


MAX_SEQ_LENGTH = 256
# The pad token id. Change according to your pretrained tokenizer
PAD_TOKEN_ID = 4


def load_tokenizer(tokenizer_file: str):
    # load our pre-trained WordLevel tokenizer, trained using a Morfessor-based
    # split
    tokenizer = Tokenizer.from_file(tokenizer_file)

    tokenizer.normalizer = normalizers.Sequence(
        [normalizers.NFKC(), normalizers.Lowercase(), normalizers.Replace(Regex("\s+"), " ")]
    )
    # we need to reconfigure the pre-tokenizer, because it can't be serialized
    # when saving from a trained model
    tokenizer.pre_tokenizer = pre_tokenizers.Sequence(
        [
            pre_tokenizers.Whitespace(),
            pre_tokenizers.Digits(individual_digits=True),
            pre_tokenizers.PreTokenizer.custom(CustomPretokenizer()),
        ]
    )

    tokenizer.post_processor = processors.BertProcessing(
        sep=("[SEP]", 1), cls=("[CLS]", 2)
    )

    # our pre-tokenizer splits in the same format as WordPiece, so we need to
    # decode it as such
    tokenizer.decoder = decoders.WordPiece()

    # tell the tokenizer the [PAD] token id, as well as max-length to keep all
    # examples at the same size
    # for efficient training
    tokenizer.enable_padding(
        pad_id=PAD_TOKEN_ID, pad_to_multiple_of=8, length=MAX_SEQ_LENGTH
    )
    tokenizer.enable_truncation(max_length=MAX_SEQ_LENGTH)

    # this wraps the tokenizer in a custom sub-class of PreTrainedTokenizerFast,
    # which is necessary so that the tokenizer plays nice with the
    # transformers's data collator classes
    tokenizer = MorphSegmentationTokenizer(tokenizer)
    return tokenizer
