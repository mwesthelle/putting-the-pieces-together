import multiprocessing
from dataclasses import dataclass, field
from typing import Optional

task_to_keys = {
    "cola": ("sentence", None),
    "mnli": ("premise", "hypothesis"),
    "assin_rte": ("premise", "hypothesis"),
    "assin_sts": ("premise", "hypothesis"),
    "mrpc": ("sentence1", "sentence2"),
    "qnli": ("question", "sentence"),
    "qqp": ("question1", "question2"),
    "rte": ("sentence1", "sentence2"),
    "sst2": ("sentence", None),
    "stsb": ("sentence1", "sentence2"),
    "wnli": ("sentence1", "sentence2"),
}


@dataclass
class DataTrainingArguments:
    """
    Arguments pertaining to what data we are going to input our model for training and
    eval.
    """

    task_name: Optional[str] = field(
        default=None,
        metadata={
            "help": f"The name of the task to train on: {', '.join(task_to_keys)}"
        },
    )
    preprocess_data: bool = field(
        default=True,
        metadata={
            "help": (
                "Whether to preprocess data before training. Set to false if you've"
                "already preprocessed the data as a separate step."
            )
        },
    )
    dataset_clip_percentage: Optional[int] = field(
        default=100, metadata={"help": "How much of the dataset to keep for training."}
    )
    dataset_name: Optional[str] = field(
        default=None,
        metadata={"help": "The name of the dataset to use (via the datasets library)."},
    )
    dataset_config_name: Optional[str] = field(
        default=None,
        metadata={
            "help": (
                "The configuration name of the dataset to use (via the datasets"
                "library)."
            )
        },
    )
    train_file: Optional[str] = field(
        default=None, metadata={"help": "The input training data file (a text file)."}
    )
    validation_file: Optional[str] = field(
        default=None,
        metadata={
            "help": (
                "An optional input evaluation data file to evaluate the perplexity on"
                "(a text file)."
            )
        },
    )
    test_file: Optional[str] = field(
        default=None,
        metadata={"help": "A csv or a json file containing the test data."},
    )
    kfold_cross_validation_splits: Optional[int] = field(
        default=0,
        metadata={"help": "Number of splits to use for k-fold cross-validation"},
    )
    resume_cross_validation_from_iteration: Optional[int] = field(
        default=0,
        metadata={"help": "Iteration from which to resume cross-validation"},
    )
    overwrite_cache: bool = field(
        default=False,
        metadata={"help": "Overwrite the cached training and evaluation sets"},
    )
    validation_split_percentage: Optional[int] = field(
        default=5,
        metadata={
            "help": (
                "The percentage of the train set used as validation set in case"
                "there's no validation split"
            )
        },
    )
    max_seq_length: Optional[int] = field(
        default=None,
        metadata={
            "help": (
                "The maximum total input sequence length after tokenization. Sequences"
                "longer than this will be truncated."
            )
        },
    )
    preprocessing_num_workers: Optional[int] = field(
        default=multiprocessing.cpu_count(),
        metadata={"help": "The number of processes to use for the preprocessing."},
    )
    mlm_probability: float = field(
        default=0.15,
        metadata={"help": "Ratio of tokens to mask for masked language modeling loss"},
    )
    line_by_line: bool = field(
        default=False,
        metadata={
            "help": (
                "Whether distinct lines of text in the dataset are to be handled as"
                "distinct sequences."
            )
        },
    )
    debug_cv_data_file_path_template: str = field(
        default=False,
        metadata={
            "help": "Print out cv splits"
        }
    )
    pad_to_max_length: bool = field(
        default=False,
        metadata={
            "help": (
                "Whether to pad all samples to `max_seq_length`. If False, will pad"
                "the samples dynamically when batching to the maximum length in the"
                "batch."
            )
        },
    )
    max_train_samples: Optional[int] = field(
        default=None,
        metadata={
            "help": (
                "For debugging purposes or quicker training, truncate the number of"
                "training examples to this value if set."
            )
        },
    )
    max_eval_samples: Optional[int] = field(
        default=None,
        metadata={
            "help": (
                "For debugging purposes or quicker training, truncate the number of"
                "evaluation examples to this value if set."
            )
        },
    )
    max_predict_samples: Optional[int] = field(
        default=None,
        metadata={
            "help": (
                "For debugging purposes or quicker training, truncate the number of"
                "prediction examples to this value if set."
            )
        },
    )

    def __post_init__(self):
        if (
            self.dataset_name is None
            and self.train_file is None
            and self.validation_file is None
        ):
            raise ValueError(
                "Need either a dataset name or a training/validation file."
            )
        else:
            if self.train_file is not None and self.preprocess_data:
                extension = self.train_file.split(".")[-1]
                if extension not in ["csv", "json", "txt"]:
                    raise ValueError(
                        "`train_file` should be a csv, a json or a txt file."
                    )
            if self.validation_file is not None and self.preprocess_data:
                extension = self.validation_file.split(".")[-1]
                if extension not in ["csv", "json", "txt"]:
                    raise ValueError(
                        "`validation_file` should be a csv, a json or a txt file."
                    )
        if not self.preprocess_data and not (self.train_file and self.validation_file):
            raise ValueError("Need preprocessed training and validation files.")
