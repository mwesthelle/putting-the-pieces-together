import pathlib
from argparse import ArgumentParser
from functools import lru_cache
from typing import List, cast

from polyglot.text import Word
import tokenizers
from tokenizers import models as tokenizer_models
from tokenizers import normalizers
from tokenizers import pre_tokenizers
from tokenizers.pre_tokenizers import Digits, PreTokenizer, Whitespace
from tokenizers.trainers import WordLevelTrainer

prefixes = {
    "a",
    "anti",
    "bi",
    "circum",
    "contra",
    "des",
    "em",
    "en",
    "es",
    "ex",
    "entre",
    "hepta",
    "hexa",
    "hiper",
    "in",
    "mega",
    "micro",
    "mini",
    "mono",
    "nano",
    "nonea",
    "octa",
    "pan",
    "penta",
    "pre",
    "pré",
    "pro",
    "pró",
    "pós",
    "pos",
    "re",
    "retro",
    "sobre",
    "sub",
    "super",
    "tetra",
    "tri",
    "ultra",
}


class CustomPretokenizer:
    @lru_cache(maxsize=200_000)
    def morfessor_split(self, i, normalized_string: tokenizers.NormalizedString):
        """[summary]

        :param i: index
        :type i: int
        :param normalized_string: a normalized_string from the `tokenizers` lib,
                                  representing a word
        :type normalized_string: tokenizers.NormalizedString
        :return: a list of substrings, resulting from splitting the word into segments
                 using a Morfessor model
        :rtype: [type]
        """
        splits = []
        word = Word(str(normalized_string), language="pt")
        beginning_of_word = 0
        morphemes = cast(List[str], word.morphemes)
        morpheme_lengths = [len(morpheme) for morpheme in morphemes]
        morphemes_count = len(morphemes)
        for idx, length in enumerate(morpheme_lengths, 1):
            end_of_word = beginning_of_word + length
            morpheme: tokenizers.NormalizedString = normalized_string[
                beginning_of_word:end_of_word
            ]
            beginning_of_word = end_of_word
            if idx == 1:
                splits.append(morpheme)
            else:
                # we'd like to treat prefixed words the same as if they were
                # unprefixed, e.g. "interessante" is a component in "superinteressante"
                # and shouldn't be modified due to its prefix.
                if str(morpheme) in prefixes and idx != morphemes_count:
                    splits.append(morpheme)
                else:
                    morpheme.prepend("##")
                    splits.append(morpheme)
        return splits

    def pre_tokenize(self, pretok: tokenizers.PreTokenizedString):
        pretok.split(self.morfessor_split)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-i", "--input-dataset-path", required=True)
    parser.add_argument("-o", "--output", required=True)
    parser.add_argument("--vocab-size", type=int, default=70_000)
    args = parser.parse_args()
    tokenizer = tokenizers.Tokenizer(tokenizer_models.WordLevel(unk_token="[UNK]"))
    tokenizer.normalizer = normalizers.Sequence(
        [
            normalizers.NFKC(),
            normalizers.Lowercase(),
            normalizers.Replace(tokenizers.Regex("\s+"), " "),
        ]
    )
    tokenizer.pre_tokenizer = pre_tokenizers.Sequence(
        [
            Whitespace(),
            Digits(individual_digits=True),
            PreTokenizer.custom(CustomPretokenizer()),
        ]
    )
    trainer = WordLevelTrainer(
        vocab_size=70_000, special_tokens=["[UNK]", "[SEP]", "[CLS]", "[MASK]", "[PAD]"]
    )
    datasets_path = pathlib.Path(args.input_dataset_path)
    files = [str(file_) for file_ in datasets_path.glob("**/*.txt")]
    tokenizer.train(files, trainer)
    # Change pre-tokenizer to a placeholder one to work around not being able to
    # serialize a custom pre-tokenizer
    tokenizer.pre_tokenizer = Whitespace()
    tokenizer.save(args.output)
