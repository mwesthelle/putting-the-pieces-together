import pathlib
from functools import lru_cache
from typing import List, cast

from polyglot.text import Word

import tokenizers
from tokenizers import normalizers
from tokenizers.models import WordLevel
from tokenizers.pre_tokenizers import Digits, PreTokenizer, Whitespace
from tokenizers.trainers import WordLevelTrainer

prefixes = {
    "a",
    "anti",
    "bi",
    "circum",
    "contra",
    "des",
    "em",
    "en",
    "es",
    "ex",
    "entre",
    "hepta",
    "hexa",
    "hiper",
    "in",
    "mega",
    "micro",
    "mini",
    "mono",
    "nano",
    "nonea",
    "octa",
    "pan",
    "penta",
    "pre",
    "pré",
    "pro",
    "pró",
    "pós",
    "pos",
    "re",
    "retro",
    "sobre",
    "sub",
    "super",
    "tetra",
    "tri",
    "ultra",
}


class CustomPretokenizer:
    @lru_cache(maxsize=200_000)
    def morfessor_split(self, i, normalized_string: tokenizers.NormalizedString):
        splits = []
        word = Word(str(normalized_string), language="pt")
        beginning_of_word = 0
        morphemes = cast(List[str], word.morphemes)
        morpheme_lengths = [len(morpheme) for morpheme in morphemes]
        prepend_next_with_double_pound = True
        for idx, length in enumerate(morpheme_lengths, 1):
            end_of_word = beginning_of_word + length
            morpheme: tokenizers.NormalizedString = normalized_string[
                beginning_of_word:end_of_word
            ]
            beginning_of_word = end_of_word
            if idx == 1:
                splits.append(morpheme)
                if str(morpheme) in prefixes:
                    prepend_next_with_double_pound = False
            else:
                # we'd like to treat prefixed words the same as if they were
                # unprefixed, e.g. "interessante" is a component in "superinteressante"
                # and shouldn't be modified due to its prefix.
                if not prepend_next_with_double_pound and idx == 2:
                    splits.append(morpheme)
                    prepend_next_with_double_pound = True
                else:
                    morpheme.prepend("##")
                    splits.append(morpheme)
        return splits

    def pre_tokenize(self, pretok: tokenizers.PreTokenizedString):
        pretok.split(self.morfessor_split)


if __name__ == "__main__":
    tokenizer = tokenizers.Tokenizer(WordLevel(unk_token="[UNK]"))
    tokenizer.normalizer = normalizers.Sequence(
        [normalizers.NFKC(), normalizers.Lowercase(), normalizers.Replace(tokenizers.Regex("\s+"), " ")]
    )
    tokenizer.pre_tokenizer = tokenizers.pre_tokenizers.Sequence(
        [
            Whitespace(),
            Digits(individual_digits=True),
            PreTokenizer.custom(CustomPretokenizer()),
        ]
    )
    trainer = WordLevelTrainer(
        vocab_size=70_000, special_tokens=["[UNK]", "[SEP]", "[CLS]", "[MASK]", "[PAD]"]
    )
    wiki_path = (
        pathlib.Path("/mnt")
        / "f"
        / "Matheus"
        / "UFRGS"
        / "Research"
        / "datasets"
        / "split_ptwiki"
    )
    wiki_corpus = [str(file) for file in wiki_path.glob("*.txt")]
    tokenizer.train(wiki_corpus, trainer)
    # Change pre-tokenizer to a placeholder one to workaround not being able to
    # serialize a custom pre-tokenizer
    tokenizer.pre_tokenizer = Whitespace()
    tokenizer.save("wiki_tokenizer_pt.json")
